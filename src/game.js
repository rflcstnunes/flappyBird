// import Bird from './components/Bird';

const sprites = new Image();
sprites.src = '/src/sprites.png';

const canvas = document.querySelector('canvas');
const contex = canvas.getContext('2d');

function collision(flappyBird, floor) {
    const flappyBirdY = flappyBird.y + flappyBird.height;
    const floorY = floor.y;

    if (flappyBirdY >= floorY) {
        return true;
    } else {
        return false;
    }
}

function createFlappyBird () {
    const flappyBird = {
        spriteX: 0,
        spriteY: 0,
        width: 33,
        height: 24,
        x: 10,
        y: 50,
        gravity: 0.25,
        speed: 0,
        jump: 4.6, // configuração do pulo
    
        toJump() {
            flappyBird.speed = - flappyBird.jump;
        },
    
        update() {
            if (collision(flappyBird, floor)) {
                // console.log('have collision');
    
                changeScreen(screens.START);
                return;
            }
    
    
    
            flappyBird.speed = flappyBird.speed + flappyBird.gravity;
            flappyBird.y = flappyBird.y + flappyBird.speed;
         // console.log(flappyBird.speed);
        },
        
        drawing() {
            contex.drawImage(
                sprites,
                flappyBird.spriteX, flappyBird.spriteY,
                flappyBird.width, flappyBird.height,
                flappyBird.x, flappyBird.y,
                flappyBird.width, flappyBird.height,
            );
        }
    }
    return flappyBird;
}

const background = {
    spriteX: 390,
    spriteY: 0,
    width: 275,
    height: 204,
    x: 0,
    y: canvas.height - 204,

    drawing() {
        contex.fillStyle = '#70c5ce';
        contex.fillRect(0, 0, canvas.width, canvas.height);

        contex.drawImage(
            sprites,
            background.spriteX, background.spriteY,
            background.width, background.height,
            background.x, background.y,
            background.width, background.height
        );

        contex.drawImage(
            sprites,
            background.spriteX, background.spriteY,
            background.width, background.height,
            (background.x + background.height), background.y,
            background.width, background.height
        );
    }
}

// floor
const floor = {
    spriteX: 0,
    spriteY: 610,
    width: 224,
    height: 112,
    x: 0,
    y: canvas.height - 112,

    update() {
        const moveFloor = 1;
        const repetIn = floor.height / 2;
        const movement = floor.x - moveFloor;

        floor.x = movement % repetIn;
    },

    drawing() {
        contex.drawImage(
            sprites,
            floor.spriteX, floor.spriteY,
            floor.width, floor.height,
            floor.x, floor.y,
            floor.width, floor.height
        );
        contex.drawImage(
            sprites,
            floor.spriteX, floor.spriteY,
            floor.width, floor.height,
            (floor.x + floor.width), floor.y,
            floor.width, floor.height
        );
    },
};

const messageReady = {
    sX: 134,
    sY: 0,
    w:174,
    h: 152,
    x: (canvas.width / 2) - 172 / 2,
    y: 50,

    drawing() {
        contex.drawImage(
            sprites,
            messageReady.sX, messageReady.sY,
            messageReady.w, messageReady.h,
            messageReady.x, messageReady.y,
            messageReady.w, messageReady.h
        );
    }
}

const global = {};
let screenActive = {};

function changeScreen(newScreen) {
    screenActive = newScreen;

    if (screenActive.initialize) {
        screenActive.initialize();
    }
}

const screens = {
    START: {
        initialize() {
            global.flappyBird = createFlappyBird();
        },
        drawing() {
            background.drawing();
            floor.drawing();
            global.flappyBird.drawing()
            messageReady.drawing();
        },
        click() {
            changeScreen(screens.GAME);
        },
        update() {
         
        }
    }
}

screens.GAME = {
    drawing() {
        background.drawing();
        floor.drawing();
        global.flappyBird.drawing()
    },
    click() {
        global.flappyBird.toJump();
    },
    update() {
        global.flappyBird.update();
    }
}

function loop() {

    screenActive.drawing();
    screenActive.update();

    requestAnimationFrame(loop);
}

window.addEventListener('click', function() {
    if (screenActive.click) {
        screenActive.click();
    }
});

changeScreen(screens.START);
loop();